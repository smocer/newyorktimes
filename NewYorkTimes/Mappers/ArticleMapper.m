//
//  ArticleMapper.m
//  NewYorkTimes
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import "ArticleMapper.h"
#import "ArticleObject.h"


@implementation ArticleMapper


- (ArticleObject *)modelFromJson:(NSDictionary *)json {
    ArticleObject *article = [ArticleObject new];
    
    NSDictionary *headline = json[@"headline"];
    
    article.headlineMain = headline[@"main"];
    article.sectionName = [json[@"section_name"] isKindOfClass:[NSNull class]] ? nil : json[@"section_name"];
    article.subsectionName = [json[@"subsection_name"] isKindOfClass:[NSNull class]] ? nil : json[@"subsection_name"];
    article.typeOfMaterial = [json[@"type_of_material"] isKindOfClass:[NSNull class]] ? nil : json[@"type_of_material"];
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssxx"];
    
    article.pubDate = [dateFormatter dateFromString: json[@"pub_date"]];
    article.snippet = [json[@"snippet"] isKindOfClass:[NSNull class]] ? @"Tap to see details..." : json[@"snippet"];
    
    NSArray *multimedia = json[@"multimedia"];
    
    if (multimedia.count != 0) {
        for (NSDictionary *item in multimedia) {
            if ([item[@"height"] intValue] == [item[@"width"] intValue]) {
                NSString *url = [NSString stringWithFormat:@"http://www.nytimes.com/%@", item[@"url"]];
                article.imageURL = [NSURL URLWithString:url];
            }
        }
    } else {
        article.imageURL = nil;
    }
    
    
    article.articleURL = [NSURL URLWithString: json[@"web_url"]];
    
    return article;
}

@end
