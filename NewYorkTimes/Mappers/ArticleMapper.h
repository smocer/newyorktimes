//
//  ArticleMapper.h
//  NewYorkTimes
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import <Foundation/Foundation.h>


@class ArticleObject;


@interface ArticleMapper : NSObject


- (ArticleObject *)modelFromJson:(NSDictionary *)json;

@end
