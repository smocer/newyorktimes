//
//  main.m
//  NewYorkTimes
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
