//
//  ArticleObject.h
//  NewYorkTimes
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface ArticleObject : NSObject


@property (nonatomic, copy) NSString *headlineMain;
@property (nonatomic, copy) NSString *sectionName;
@property (nonatomic, copy) NSString *subsectionName;
@property (nonatomic, copy) NSString *typeOfMaterial;
@property (nonatomic, copy) NSDate *pubDate;
@property (nonatomic, copy) NSString *snippet;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSURL *articleURL;

@end
