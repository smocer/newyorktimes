//
//  Requester.m
//  new york times
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import "Requester.h"


@interface Requester ()


@property (nonatomic, strong) AFHTTPSessionManager *manager;


@end


@implementation Requester


- (AFHTTPSessionManager *)manager {
    NSURL *baseURL = [NSURL URLWithString:@"https://api.nytimes.com"];
    if (!_manager) {
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        _manager.requestSerializer = [AFJSONRequestSerializer new];
        _manager.responseSerializer = [AFJSONResponseSerializer new];
    }
    return _manager;
}

- (void)searchWithQuery:(NSString *)query pageNumber:(NSNumber *)page andCompletion:(SuccessCompletionBlock)successBlock {
    NSDictionary *params = @{@"api-key" : @"6f467c0a60d443359d8cec5d22c70aa1",
                             @"q" : query,
                             @"sort" : @"newest",
                             @"page" : page
                             };
    [self.manager GET:@"/svc/search/v2/articlesearch.json" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"SUCCESS!");
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Something went wrong. Reason: %@", [error localizedDescription]);
    }];
}

@end
