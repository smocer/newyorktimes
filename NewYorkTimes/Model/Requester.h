//
//  Requester.h
//  new york times
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import <AFNetworking/AFNetworking.h>


typedef void (^SuccessCompletionBlock)(id response);
typedef void (^FailureCompletionBlock)(NSError *error);


@interface Requester : NSObject


- (void)searchWithQuery:(NSString *)query pageNumber:(NSNumber *)page andCompletion:(SuccessCompletionBlock)successBlock;

@end
