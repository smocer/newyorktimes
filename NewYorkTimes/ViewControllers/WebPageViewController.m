//
//  WebPageViewController.m
//  NewYorkTimes
//
//  Created by Admin on 30.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import <WebKit/WebKit.h>
#import "WebPageViewController.h"
#import "ASIWebPageRequest.h"
#import "ASIDownloadCache.h"
#import "SavedArticle+CoreDataClass.h"
#import "AppDelegate.h"


@interface WebPageViewController () <WKUIDelegate, WKNavigationDelegate>

//UI
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (nonatomic, strong) WKWebView *webView;

//data
@property (nonatomic, strong) ArticleObject *theOldArticle;
@property (nonatomic, strong) SavedArticle *theOldSavedArticle;
@property (nonatomic, strong) ASIWebPageRequest *request;

//custom activity indicator
@property (nonatomic, strong) UIActivityIndicatorView *spinner;
@property (nonatomic, strong) UIView *container;
@property (nonatomic, strong) UIView *loadingView;
@property (nonatomic, strong) UILabel *loadingLabel;

@end


@implementation WebPageViewController


#pragma mark - Lazy

- (UIActivityIndicatorView *)spinner {
    if (!_spinner) {
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    return _spinner;
}

- (ArticleObject *)theOldArticle {
    if (!_theOldArticle) {
        _theOldArticle = [ArticleObject new];
    }
    return _theOldArticle;
}

- (WKWebView *)webView {
    if (!_webView) {
        WKWebViewConfiguration *config = [WKWebViewConfiguration new];
        config.ignoresViewportScaleLimits = YES;
        _webView = [[WKWebView alloc] initWithFrame:self.view.frame configuration:config];
        _webView.UIDelegate = self;
        _webView.navigationDelegate = self;
    }
    return _webView;
}


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"WebPageVC viewDidLoad");
    
    [self.view addSubview:self.webView];
    self.container = [[UIView alloc] initWithFrame:self.view.frame];
    self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    self.loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 57, 60, 17)];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.pageType == NYTWebPageTypeOnline) {
        [self configureViewForOnline];
    } else {
        [self configureViewForOffline];
    }
}


#pragma mark - Actions

- (IBAction)saveButtonTapped:(id)sender {
    [self saveHTMLPageWithURL:self.webView.URL];
}

- (IBAction)handlePinchGesture:(UIPinchGestureRecognizer *)sender {
    self.webView.scrollView.zoomScale *= sender.scale;
    sender.scale = 1;
}

#pragma mark - Private

//* Activity indicator *//
- (void)showCustomActivityIndicatorOnView {
    
    self.loadingLabel.text = @"Loading...";
    self.loadingLabel.textColor = [UIColor whiteColor];
    self.loadingLabel.textAlignment = NSTextAlignmentCenter;
    self.loadingLabel.adjustsFontSizeToFitWidth = YES;
    
    self.container.center = self.view.center;
    self.container.backgroundColor = [[UIColor alloc] initWithRed:1 green:1 blue:1 alpha:0.3];
    
    
    self.loadingView.center = self.view.center;
    self.loadingView.backgroundColor = [[UIColor alloc] initWithRed:68/255 green:68/255 blue:68/255 alpha:0.7];
    self.loadingView.clipsToBounds = YES;
    self.loadingView.layer.cornerRadius = 10;
    
    self.spinner.frame = CGRectMake(0, 0, 40, 40);
    self.spinner.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    self.spinner.center = CGPointMake(self.loadingView.frame.size.width / 2, self.loadingView.frame.size.height / 2);
    
    self.loadingLabel.userInteractionEnabled = NO;
    self.container.userInteractionEnabled = NO;
    self.loadingView.userInteractionEnabled = NO;
    self.spinner.userInteractionEnabled = NO;
    
    [self.loadingView addSubview:self.spinner];
    [self.loadingView addSubview:self.loadingLabel];
    [self.container addSubview:self.loadingView];
    [self.view addSubview:self.container];
    
    [self.spinner startAnimating];
    [self.view becomeFirstResponder];
}

- (void)hideCustomActivityIndicator {
    [self.spinner stopAnimating];
    [self.container removeFromSuperview];
}



//* Preparing after segue *//

- (void)configureViewForOnline {
    self.saveButton.enabled = YES;
    self.saveButton.tintColor = [UIColor blueColor];
    self.title = self.theNewArticle.headlineMain;
    
    if (self.theNewArticle) {
        if (self.theNewArticle != self.theOldArticle) {
            self.theOldArticle = self.theNewArticle;
            
            [self showCustomActivityIndicatorOnView];
            
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:self.theNewArticle.articleURL];
            
            [self.webView loadRequest:request];
        }
    }
}

- (void)configureViewForOffline {
    self.saveButton.enabled = NO;
    self.saveButton.tintColor = [UIColor clearColor];
    self.title = self.theNewOfflineArticle.title;
    
    if (self.theNewOfflineArticle) {
        if (![self.theNewOfflineArticle.title isEqualToString: self.theOldSavedArticle.title]) {
            self.theOldSavedArticle = self.theNewOfflineArticle;
            
            [self showCustomActivityIndicatorOnView];
            
            NSString *html = [[NSString alloc] initWithData:self.theNewOfflineArticle.sourceHTML encoding:NSUTF8StringEncoding];
            NSURL *baseURL = [NSURL URLWithString:self.theNewOfflineArticle.baseURL];
            
            [self.webView loadHTMLString:html baseURL:baseURL];
        }
    }
}


//* Page saving *//

- (void)saveHTMLPageWithURL:(NSURL *)pageURL {
    self.request.delegate = nil;
    [self.request cancel];
    
    self.request = [ASIWebPageRequest requestWithURL:pageURL];
    self.request.delegate = self;
    self.request.didFailSelector = @selector(webPageFetchFailed:);
    self.request.didFinishSelector = @selector(webPageFetchSucceeded:);
    
    // Tell the request to embed external resources directly in the page
    self.request.urlReplacementMode = ASIReplaceExternalResourcesWithData;
    
    // It is strongly recommended you use a download cache with ASIWebPageRequest
    // When using a cache, external resources are automatically stored in the cache
    // and can be pulled from the cache on subsequent page loads
    self.request.downloadCache = [ASIDownloadCache sharedCache];
    
    // Ask the download cache for a place to store the cached data
    // This is the most efficient way for an ASIWebPageRequest to store a web page
    self.request.downloadDestinationPath = [[ASIDownloadCache sharedCache] pathToStoreCachedResponseDataForRequest:self.request];
    NSLog(@"downloadDestinationPath = %@", [[ASIDownloadCache sharedCache] pathToStoreCachedResponseDataForRequest:self.request]);
    
    [self.request startAsynchronous];
}

- (void)webPageFetchFailed:(ASIHTTPRequest *)theRequest
{
    // Obviously you should handle the error properly...
    NSLog(@"Fail: %@",theRequest.error);
}

- (void)webPageFetchSucceeded:(ASIHTTPRequest *)theRequest
{
    NSString *response = [NSString stringWithContentsOfFile:
                          [theRequest downloadDestinationPath] encoding:[theRequest responseEncoding] error:nil];
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    NSManagedObjectContext *context = appDelegate.persistentContainer.viewContext;
    
    SavedArticle *savedArticle = [[SavedArticle alloc] initWithContext:context];
    
    savedArticle.title = self.theNewArticle.headlineMain;
    savedArticle.snippet = self.theNewArticle.snippet;
    savedArticle.baseURL = [self.request.url absoluteString];
    savedArticle.sourceHTML = data;
    
    if ([context save:nil]) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success!"
                                                                       message:@"The page has been saved."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:nil];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


#pragma mark - WKNavigationDelegate

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [self hideCustomActivityIndicator];
}

@end
