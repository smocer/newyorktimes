//
//  SavedArticlesViewController.m
//  NewYorkTimes
//
//  Created by Admin on 01.07.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import "SavedArticlesViewController.h"
#import "SavedArticle+CoreDataClass.h"
#import "AppDelegate.h"
#import "OfflineArticleTableViewCell.h"
#import "WebPageViewController.h"


//NYT = NewYorkTimes
static NSString *const NYTOfflineArticleTableViewCellNibName = @"OfflineArticleTableViewCell";
static NSString *const NYTOfflineArticleTableViewCellReuseID = @"OfflineArticleTableViewCellReuseID";
static NSString *const NYTShowOfflineArticleSegueID = @"show offline article";


@interface SavedArticlesViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSMutableArray<SavedArticle *> *articles;
@property (nonatomic, strong) SavedArticle *articleToShow;

@end


@implementation SavedArticlesViewController


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Favourites";
    [self setUpOutlets];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:NYTShowOfflineArticleSegueID]) {
        WebPageViewController *webvc = segue.destinationViewController;
        webvc.theNewOfflineArticle = self.articleToShow;
        webvc.pageType = NYTWebPageTypeOffline;
    }
}


#pragma mark - lazy initiation

- (NSMutableArray *)articles {
    if (!_articles) {
        _articles = [@[] mutableCopy];
    }
    return _articles;
}

- (NSManagedObjectContext *)context {
    if (!_context) {
        AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
        _context = appDelegate.persistentContainer.viewContext;
    }
    return _context;
}

#pragma mark - Private

- (void)setUpOutlets {
    
    //table view
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    UINib *nib = [UINib nibWithNibName:NYTOfflineArticleTableViewCellNibName bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:NYTOfflineArticleTableViewCellReuseID];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
}

- (void)reloadData {
    NSFetchRequest *request = [SavedArticle fetchRequest];
    self.articles = [[self.context executeFetchRequest:request error:nil] mutableCopy];
    [self.tableView reloadData];
}

- (void)deleteSavedArticle:(SavedArticle *)article {
    [self.articles removeObject:article];
    [self.context deleteObject:article];
    NSError * error = nil;
    if (![self.context save:&error])
    {
        NSLog(@"Error ! %@", error.localizedDescription);
    }
// TODO: С точки зрения пользователя все будет работать отлично, но стоит также добавить удаление файлов, связанных с данной страницей, из памяти устройства.
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.articles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OfflineArticleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NYTOfflineArticleTableViewCellReuseID];
    SavedArticle *article = self.articles[indexPath.row];
    
    [cell configureWithDatabaseModel:article];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        SavedArticle *articleToRemove = self.articles[indexPath.row];
        [self deleteSavedArticle:articleToRemove];
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
        [tableView endUpdates];
    }
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.articleToShow = self.articles[indexPath.row];
    [self performSegueWithIdentifier:NYTShowOfflineArticleSegueID sender:nil];
}

@end
