//
//  SearchViewController.m
//  NewYorkTimes
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import "SearchViewController.h"
#import "ArticleObject.h"
#import "ArticleMapper.h"
#import "ArticleTableViewCell.h"
#import "ArticleNoImageTableViewCell.h"
#import "Requester.h"
#import "WebPageViewController.h"


//NYT = NewYorkTimes
static NSString *const NYTArticleTableViewCellNibName = @"ArticleTableViewCell";
static NSString *const NYTArticleTableViewCellReuseID = @"ArticleTableViewCellReuseID";
static NSString *const NYTArticleNoImageTableViewCellNibName = @"ArticleNoImageTableViewCell";
static NSString *const NYTArticleNoImageTableViewCellReuseID = @"ArticleNoImageTableViewCellReuseID";
static NSString *const NYTShowArticleSegueID = @"show article";


@interface SearchViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating>


@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UISearchController *searchController;
@property (nonatomic, strong) UIActivityIndicatorView *spinner;

@property (nonatomic, strong) NSMutableArray<ArticleObject *> *articles;
@property (nonatomic, strong) ArticleObject *articleToShow;
@property (nonatomic, strong) NSTimer *timer;

@property (nonatomic, strong) NSNumber *pageToLoad;
@property (nonatomic, assign) BOOL isObtaining;

@end


@implementation SearchViewController


#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Article search";
    self.isObtaining = NO;
    [self setUpOutlets];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:NYTShowArticleSegueID]) {
        WebPageViewController *webvc = segue.destinationViewController;
        webvc.theNewArticle = self.articleToShow;
        webvc.pageType = NYTWebPageTypeOnline;
    }
}


#pragma mark - lazy initiation

- (NSMutableArray *)articles {
    if (!_articles) {
        _articles = [@[] mutableCopy];
    }
    return _articles;
}

- (NSNumber *)pageToLoad {
    if (!_pageToLoad) {
        return @0;
    }
    return _pageToLoad;
}


#pragma mark - Private

- (void)setUpOutlets {
    
    //table view
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    UINib *nib = [UINib nibWithNibName:NYTArticleTableViewCellNibName bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:NYTArticleTableViewCellReuseID];
    
    UINib *nibNoImg = [UINib nibWithNibName:NYTArticleNoImageTableViewCellNibName bundle:nil];
    [self.tableView registerNib:nibNoImg forCellReuseIdentifier:NYTArticleNoImageTableViewCellReuseID];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 150;
    
    //search controller
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.tableView.tableHeaderView = self.searchController.searchBar;
    self.searchController.searchBar.delegate = self;
    
    //spinner
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.spinner.center = self.tableView.center;
    self.spinner.hidesWhenStopped = YES;
}

- (void)searchWithNewText {
    self.pageToLoad = @0;
    [self searchForRequest:self.searchController.searchBar.text];
}

- (void)searchForRequest:(NSString *)request {
    if (request.length < 3) {
        return;
    }
    self.isObtaining = YES;
    [self.spinner startAnimating];
    
    if ([self.pageToLoad isEqualToNumber: @0]) {
        [self.articles removeAllObjects];
        [self.tableView reloadData];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        Requester *requester = [Requester new];
        [requester searchWithQuery:request pageNumber:self.pageToLoad andCompletion:^(id response) {
            NSDictionary *responseDictionary = (NSDictionary *)response;
            NSArray *docs = ((NSDictionary *)responseDictionary[@"response"])[@"docs"];
            
            ArticleMapper *mapper = [ArticleMapper new];
            for (NSDictionary *json in docs) {
                ArticleObject *article = [mapper modelFromJson:json];
                if (article) {
                    [self.articles addObject:article];
                }
            }
            
            self.isObtaining = NO;
            [self.tableView reloadData];
            [self.spinner stopAnimating];
        }];
    });
    
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.articles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ArticleObject *article = self.articles[indexPath.row];
    if (article.imageURL) {
        ArticleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NYTArticleTableViewCellReuseID];
        [cell configureWithArticleObject:article];
        return cell;
    } else {
        ArticleNoImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NYTArticleNoImageTableViewCellReuseID];
        [cell configureWithArticleObject:article];
        return cell;
    }
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.articleToShow = self.articles[indexPath.row];
    [self performSegueWithIdentifier:NYTShowArticleSegueID sender:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    BOOL didScrollToBottom = scrollView.contentOffset.y > scrollView.contentSize.height - 2 * CGRectGetHeight(scrollView.frame);
    if (didScrollToBottom) {
        if (self.isObtaining) {
            return;
        }
        self.pageToLoad = @([self.pageToLoad intValue] + 1);
        [self searchForRequest:self.searchController.searchBar.text];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"textDidChange");
    [self.timer invalidate];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(searchWithNewText) userInfo:nil repeats:NO];
}


#pragma mark - UISearchResultsUpdating

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
    //[self searchForRequest:searchController.searchBar.text];
    //[self.timer invalidate];
    //self.timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(search) userInfo:nil repeats:NO];
}

@end
