//
//  WebPageViewController.h
//  NewYorkTimes
//
//  Created by Admin on 30.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ArticleObject.h"
#import "SavedArticle+CoreDataClass.h"


typedef NS_ENUM(NSUInteger, NYTWebPageType) {
    NYTWebPageTypeOnline,
    NYTWebPageTypeOffline
};


@interface WebPageViewController : UIViewController

@property (nonatomic, assign) NYTWebPageType pageType;
@property (nonatomic, strong) SavedArticle *theNewOfflineArticle;
@property (nonatomic, strong) ArticleObject *theNewArticle;

@end
