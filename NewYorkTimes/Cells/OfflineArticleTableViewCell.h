//
//  OfflineArticleTableViewCell.h
//  NewYorkTimes
//
//  Created by Admin on 01.07.17.
//  Copyright © 2017 sbt. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SavedArticle+CoreDataClass.h"

@interface OfflineArticleTableViewCell : UITableViewCell

- (void)configureWithDatabaseModel:(SavedArticle *)article;

@end
