//
//  ArticleNoImageTableViewCell.h
//  NewYorkTimes
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 sbt. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "ArticleObject.h"


@interface ArticleNoImageTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *sectionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *subsectionNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeOfMaterialLabel;
@property (weak, nonatomic) IBOutlet UILabel *pubDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *snippetLabel;

- (void)configureWithArticleObject:(ArticleObject *)article;

@end
