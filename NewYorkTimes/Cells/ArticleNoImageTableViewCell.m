//
//  ArticleNoImageTableViewCell.m
//  NewYorkTimes
//
//  Created by Admin on 07.07.17.
//  Copyright © 2017 sbt. All rights reserved.
//

#import "ArticleNoImageTableViewCell.h"

@implementation ArticleNoImageTableViewCell


- (void)configureWithArticleObject:(ArticleObject *)article {
    self.headerLabel.text = article.headlineMain;
    self.sectionNameLabel.text = article.sectionName;
    self.subsectionNameLabel.text = article.subsectionName;
    self.typeOfMaterialLabel.text = article.typeOfMaterial;
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssxx"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    self.pubDateLabel.text = [dateFormatter stringFromDate: article.pubDate];
    self.snippetLabel.text = article.snippet;
}


@end
