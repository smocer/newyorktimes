//
//  OfflineArticleTableViewCell.m
//  NewYorkTimes
//
//  Created by Admin on 01.07.17.
//  Copyright © 2017 sbt. All rights reserved.
//

#import "OfflineArticleTableViewCell.h"

@interface OfflineArticleTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *snippetLabel;

@end

@implementation OfflineArticleTableViewCell

- (void)configureWithDatabaseModel:(SavedArticle *)article {
    self.titleLabel.text = article.title;
    self.snippetLabel.text = article.snippet;
}

@end
