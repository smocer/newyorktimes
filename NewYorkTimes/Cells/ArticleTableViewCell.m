//
//  ArticleTableViewCell.m
//  NewYorkTimes
//
//  Created by Admin on 29.06.17.
//  Copyright © 2017 sbt. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "ArticleTableViewCell.h"


@implementation ArticleTableViewCell


- (void)configureWithArticleObject:(ArticleObject *)article {
    self.headerLabel.text = article.headlineMain;
    self.sectionNameLabel.text = article.sectionName;
    self.subsectionNameLabel.text = article.subsectionName;
    self.typeOfMaterialLabel.text = article.typeOfMaterial;
    
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssxx"];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    self.pubDateLabel.text = [dateFormatter stringFromDate: article.pubDate];
    self.snippetLabel.text = article.snippet;
    [self.picture setShowActivityIndicatorView:YES];
    [self.picture setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [self.picture sd_setImageWithURL: article.imageURL];
}

@end
